//TABS Payload Decoder

function Decoder(bytes, port) 
  {
    var retValue =   { 
      bytes: bytes
    };
// Docoder für doornwindow
/*
Port            100
Payload Length  8 bytes

Byte    0       1       2       3       4       5       6       7
Field   Status  Battery Temp    Time    Time    NA      Count   NA


*/
    if (port == 100) {   

        retValue.port = 100;
        retValue.battery_state = 100 * ((bytes[1]>>4)/ 15);
        retValue.battery_voltage = (25 + (bytes[1]&&4)) / 10;
        retValue.state = bytes[0]&&1;
        if (retValue.state == 1) {
            retValue.contact = "open";
        } else if (retValue.state === 0) {
            retValue.contact = "closed";
        }
        retValue.temperature = (bytes[2]) -32;
        retValue.time_elapsed_since_trigger = ((bytes[3]<<8) | (bytes[4]));
        retValue.total_count = ((bytes[5]<<16) | (bytes[6]<<8) | (bytes[7]));
     
    return retValue; 
    }


// Docoder für healthy_home
// Testwert 08FB3730FFFFFFFF
    if (port == 103) {

        retValue.port = 103;
        retValue.battery_state = 100 * ((bytes[1]>>4)/ 15);
        retValue.battery_voltage = (25 + (bytes[1]&&4)) / 10;
        retValue.temperature = (bytes[2]) -32;
        retValue.relative_humidity = (bytes[3]);
        retValue.co2 = (bytes[4]<< 8) | (bytes[5]);
        retValue.voc = (bytes[6]<< 8) | (bytes[7]);

    return retValue; 
    }

// Docoder für motion
// Testwert 00FB361D06310400
    if (port == 102) {

        retValue.port = 102;
        retValue.battery_state = 100 * ((bytes[1]>>4)/ 15);
        retValue.battery_voltage = (25 + (bytes[1]&&4)) / 10;
        retValue.temperature = (bytes[2]) -32;
        retValue.total_count = ((bytes[5]<<16) | (bytes[6]<<8) | (bytes[7]));
        retValue.state = bytes[0]&&1;
        if (retValue.state == 1) {
            retValue.contact = "occupied";
        } else if (retValue.state === 0) {
            retValue.contact = "free";
        }

    return retValue; 
    }

// Docoder für object_locator
    if (port == 136) {
       
        retValue.port = 136;
        retValue.battery_state = 100 * ((bytes[1]>>4)/ 15);
        retValue.battery_voltage = (25 + (bytes[1]&&4)) / 10;
        retValue.temperature = (bytes[2]) -32;

 //Testdaten
        retValue.acc= 256;
        retValue.button= 0;
        retValue.button_state= "not_pushed";
        retValue.gnss_fix= 1;
        retValue.gnss_state= "no_fix";
        retValue.latitude= 47.436121;
        retValue.longitude= 8.475262;

        retValue.location= [8.475262, 47.436121];
    return retValue; 
    }


// Docoder für pushbutton
    if (port == 147) {
        
        retValue.port = 147;
        retValue.total_count = ((bytes[5]<<16) | (bytes[6]<<8) | (bytes[7]));
        retValue.battery_state = 100 * ((bytes[1]>>4)/ 15);
        retValue.battery_voltage = (25 + (bytes[1]&&4)) / 10;
        retValue.temperature = (bytes[2]) -32;        
//Testdaten
        retValue.time_elapsed_since_trigger= 234;
        retValue.button_1= 0;
        retValue.button_1_state= "not_pushed";
        retValue.button_1_count= 0;
        retValue.button_0= 1;
        retValue.button_0_state= "pushed";

    return retValue; 
    }

  }